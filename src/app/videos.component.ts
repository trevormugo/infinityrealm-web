import { Component } from "@angular/core";
import { Router } from '@angular/router';
@Component({
  selector: 'videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent{
  public status:Boolean;
  public styleobj;
  public foraudio:Boolean;
  constructor( public router : Router,){}
  public side(event){
    this.status = event;
  }
  public renderstyle(){
    if(this.status === true)
    {
      //the sidebar is rendered
      this.foraudio = false;
      return {
        "background-color":"#46484c",
        "float": "right",
        "margin-top" : "71px",
        "height": "100%",
        "width": "72%",
        "display":"flex",
        "justify-content": "center",
        "align-items": "center",
      }
    }
    else
    {
      //the side bar is not rendered
      this.foraudio = true;
      return {
        "background-color":"#46484c",
        "float": "right",
        "margin-top" : "71px",
        "height": "100%",
        "width": "100%",
        "display":"flex",
        "justify-content": "center",
        "align-items": "center",
      }
    }
  }
  //we will access the above styles to style the audio player page

  //navigate to post page
  public shootsettings(){
    this.router.navigate(["/render/sharepost"]);
  }
}
