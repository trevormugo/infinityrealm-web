import { Component } from "@angular/core";
import { VideosComponent } from "./videos.component";
import { MyPlaylistComponent } from "./myplaylist.component";
import { Router } from '@angular/router';
import { Adress } from "./ip.service";
import { OnInit } from "@angular/core";
import { FetchService } from './fetching.service';
import { ActivatedRoute } from "@angular/router";
import { DOCUMENT } from '@angular/common';
import { Inject }  from '@angular/core';
import { SocketService } from './socket.service';
import { FileSelectDirective , FileUploader } from "ng2-file-upload";

@Component({
  selector : 'watch',
  templateUrl : "./watch.component.html",
  styleUrls : ["./watch.component.css"],
})

export class WatchComponent implements OnInit{
  //now we have the id now we use mataudio to play the files
  public idparam:String;
  public mycontext;
  public myidsign:String;
  public impoarray = [];
  public routetowatch;
  public yu;
  public trace;
  public tracecurr;
  public tracedurr;
  public myaud;
  public filepicitem;
  public filenameitem;
  //delete this
  public classio;
  public ans;
  public idreferer2:String = localStorage.getItem("idr");
  public idreferer:String;
  public identityofuploader:String;
  public paused:Boolean=false;
  public mysrc:String=this.myip.getIp()+"/coverimage/";
  //to read the profile image just get the id of the uploader then send it as a src
  public profileimgsrc:String=this.myip.getIp()+"/profileimgread/";
  uploader:FileUploader = new FileUploader({
    url : this.myip.getIp()+"/newcover/"+this.idparam,
    disableMultipart : false,
  });
  ngOnInit(){
    //the uploader disablecredentials till we get ssl certificate
    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
    };
    //gets the url even after you change it
    this.route.paramMap.subscribe(params => {
      this.itsedit = false;
      this.paused = false;
      this.notmuted = true;
      let audio = new Audio();
      let wm = new WeakMap();
      let context = new AudioContext();
      this.mycontext = context;
      //we wiil use this as our progess handle instead of using onprogress function
      //since when we seek we update the time we kill 2 birds with one variable
      audio.ontimeupdate=()=>{
          this.yu = audio.currentTime /  audio.duration * 100;
          //get duration in minutes and seconds
          let minutesdurr= Math.floor(audio.duration / 60);
          let secondsdurr= Math.floor(audio.duration % 60);
          //get currentTime in minutes and seconds
          let minutescurr= Math.floor(audio.currentTime / 60);
          let secondscurr= Math.floor(audio.currentTime % 60);
          let a = secondscurr.toString();
          let b = secondsdurr.toString();
          if(secondscurr < 10)
          {
            a = "0" + secondscurr.toString();
          }
          if(secondsdurr < 10)
          {
            b = "0" + secondsdurr.toString();
          }
          this.tracecurr= minutescurr+":"+a;
          this.tracedurr= minutesdurr +":"+b;
      };
      //buffering handler
      audio.onwaiting = () => {
        //console.log("buffering");
        //now we can play around with the pages color
        this.tracecurr = "buffering...";
      }
      //access the audio object in the class
      this.myaud = audio;
      this.nowvol = audio.volume;
      this.idparam = params.get("id");
      this.routetowatch = 'http://192.168.0.19:8080/reader/'+this.idparam;
      let canvas, ctx, source, analyser, fbc_array, bars, bar_x, bar_width, bar_height;
      audio.crossOrigin = "anonymous";
      audio.src = this.routetowatch;
      audio.controls = false;
      //we will set this later
      audio.loop = false;
      audio.autoplay = true;
      audio.load();
      audio.play();
      //after the meta data has been loaded check the duration
      initpl();
      function initpl(){
        document.getElementById('audio_box').appendChild(audio);
        //context = new AudioContext();
        analyser = context.createAnalyser();
        canvas = document.getElementById('analyser_render');
        ctx = canvas.getContext('2d');
        source = context.createMediaElementSource(audio);
        source.connect(analyser);
      	analyser.connect(context.destination);
        frameLooper();
      }
      function frameLooper(){
        window.requestAnimationFrame(frameLooper);
        fbc_array = new Uint8Array(analyser.frequencyBinCount);
        analyser.getByteFrequencyData(fbc_array);
        ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas
        ctx.fillStyle = '#002D3C'; // Color of the bars
        bars = 100;//number of bars
        for (var i = 0; i < bars; i++) {
          bar_x = i * 3;//the spacing of each bar
          bar_width = 2;//the width of each bar
          bar_height = -(fbc_array[i] / 2);
          //  the method that draws the bars fillRect( x, y, width, height ) // Explanation of the parameters below
          ctx.fillRect(bar_x, canvas.height, bar_width, bar_height);
        }
      }
      const fetchbone = {
        idval : this.idparam,
      };
      this.fetchservice.fecthdatatowatch(fetchbone)
          .subscribe((data) =>{
            //this is the picture
            //this is what we are watching
            //get info on what we are watching
            console.log(this.routetowatch);
            console.log(data);
            this.filepicitem = data.data.filepic;
            this.filenameitem = data.data.display;
          });
      //this is meant to retrieve the users quick info
      this.fetchservice.fetchquickinfo(fetchbone)
          .subscribe((data)=>{
            //this is the information on the upoader
            console.log(data);
            this.identityofuploader = data.data.userName;
            this.idreferer = data.data._id;
          });
    });
    this.fetchservice.fetchfunction()
        .subscribe((data) =>{
          this.impoarray.push(data.data);
          this.callback();
        });
  }

  public errorland:String;
  constructor(@Inject(DOCUMENT) document,public socket : SocketService , public myarray : MyPlaylistComponent,private route: ActivatedRoute , public router : Router , public fetchservice : FetchService , public impostyle : VideosComponent,public myip : Adress,){
    //after a file is successfully uploaded
    this.uploader.onCompleteItem = (item:any , response:any , status:any , headers:any)=>{
      //use the response data to emit to a socket then update the database from there
      var realresponse = JSON.parse(response);
      console.log(realresponse);
      this.socket.updateCover(realresponse);
    }
  }
  //list
  public list:Boolean;
  public styleobjforudio;
  public renderstyle3(){
    //this is meant to return the playlist when the side bar is rendered now let us return the sidebar when the sidebar is rendered;
    this.list = this.impostyle.foraudio;
    if(this.list === false)
    {
      //means the sidebar has been rendered
      this.styleobjforudio = {
        "width":"100%",
        "height":"100%",
        "display": "flex",
        "align-items": "center",
        "justify-content": "center",
      }
    }
    else if(this.list === true)
    {
      //means that the sidebar has not been rendered
      this.styleobjforudio = {
        "width":"100%",
        "height":"100%",
        "display": "flex",
        "align-items": "center",
        "justify-content": "center",
      }
    }
    return this.impostyle.renderstyle();
  }
  //delete this
  public classico(){
    return this.classio;
  }
  //play and pause operators
  public playpausecontrol(){
    if(this.paused === true)
    {
      this.myaud.play();
      this.paused = false;
    }
    else
    {
      this.myaud.pause();
      this.paused = true;
    }
  }
  //seeking operators
  public mouse($event){
    this.ans = this.myaud.duration * ($event.target.value / 100);
    this.myaud.currentTime = this.ans;
    console.log(this.ans);
  }

  public callback(){
    console.log(this.impoarray);
  }
  //volume operators mute
  public muted:Boolean = false;
  public notmuted:Boolean = true;
  public volumedown:Boolean = false;
  public nowvol;
  public mute(){
    this.muted = true;
    this.notmuted = false;
    this.volumedown = false;
    this.myaud.muted = true;
  }
  //unmute
  public unmute(){
    this.muted = false;
    this.notmuted = true;
    this.myaud.muted = false;
  }
  //range slider
  public changevolume($event){
    //add keyboard shortcuts
    if($event.target.value === 40 || $event.target.value < 40)
    {
      if(this.muted === false)
      {
        this.volumedown = true;
        this.notmuted = false;
      }
    }
    else if($event.target.value > 40)
    {
      if(this.muted === false)
      {
        this.volumedown = false;
        this.notmuted = true;
      }
    }
    else if($event.target.value === 0)
    {
      if(this.muted === false)
      {
        this.volumedown = false;
        this.notmuted = false;
        this.muted = true;
      }
    }
    this.myaud.volume = $event.target.value / 100;
  }
  //change the channel or song
  public change(){
    //clear the source to avoid multiple audio playing
    this.myaud.src="";
  }
  //speacial controls
  //follow icon
  public followiconplus:Boolean=false;
  public followiconminus:Boolean=true;
  public followingor;
  public followingorstyle;
  public followuser(){
    //toggele the Boolean
    this.followiconplus = !this.followiconplus;
    this.followiconminus = !this.followiconminus;
    if(this.followiconplus === true)
    {
      this.followingor = "unfollowed";
      this.followingorstyle = {
        "color": "red",
      }
    }
    else
    {
      this.followingor = "following";
      this.followingorstyle = {
        "color": "green",
      }
    }
    setTimeout(()=> {
      this.followingor = "";
    },2000);
  }
  //like icon
  public likeiconplus:Boolean=false;
  public likeiconminus:Boolean=true;
  public likeor;
  public likeorstyle;
  public likepost(){
    //toggle the Boolean
    this.likeiconplus = !this.likeiconplus;
    this.likeiconminus = !this.likeiconminus;
    if(this.likeiconplus === true)
    {
      this.likeor = "liked";
      this.likeorstyle = {
        "color" : "green",
      }
    }
    else
    {
      this.likeor = "unliked";
      this.likeorstyle = {
        "color" : "red",
      }
    }
    setTimeout(()=>{
      this.likeor = "";
    },2000);
  }
  //the eye icon is unclickable becoz it represents views
  //but it is hoverable
  public viewsiconr:Boolean = false;
  public viewstxt(){
    this.viewsiconr = true;
    setTimeout(()=>{
      this.viewsiconr = false;
    },1000);
  }
  //for the comments we will do it later because we have to check the data base to see if you have commented
  //same as the views
  public commentsr:Boolean= false;
  public commentsing(){
    this.commentsr = true;
    setTimeout(()=>{
      this.commentsr = false;
    },1000);
  }
  //my settings
  public mysettings = false;
  public rendersettings(){
    this.mysettings = !this.mysettings;
  }
  //editmode
  //click function for the edit icon
  public itsedit:Boolean = false;
  public editmode(){
    //we can only edit our own data
    if( this.idreferer !== this.idreferer2 )
    {
      this.itsedit = false;
      //this means that the client is not the uploader
      alert("forbidden");
    }
    else if( this.idreferer === this.idreferer2)
    {
      this.itsedit = !this.itsedit;
      //if this condition is met is when we can edit the information
    }
    else
    {
      this.itsedit = false;
      console.log("an error occurred");
    }
  }
  //function to show the box to edit the filename
  public showfilenamenotifbox:Boolean=false;
  public showfilenotif(){
    this.showfilenamenotifbox = true;
    this.showusernamenotifbox = false;
  }
  public hidefilenotif(){
    this.showfilenamenotifbox = false;
    this.showusernamenotifbox = false;
  }
  //function show the box to edit the username
  public showusernamenotifbox:Boolean=false;
  public shownotif(){
    this.showusernamenotifbox = true;
    this.showfilenamenotifbox = false;
  }
  public hidenotif(){
    this.showusernamenotifbox = false;
    this.showfilenamenotifbox = false;
  }
  //funtion to send the edited name and file name to the database through a socket
  public newname:String;
  public newfilenameobj:String;
  public newusername($event){
    this.newname = $event.target.value;
  }
  public submitnewname(){
    if(this.newname)
    {
      const delivernameinfo = {
        "username":this.newname,
        "id": localStorage.getItem("idr"),
      }
      this.socket.nameupdate(delivernameinfo);
      this.socket.recieveupdatesmsg()
      .subscribe((msg)=>{
        if(msg === "error")
        {
          console.log("error")
        }
        else if(msg === "no-response")
        {
          console.log("logout login")
        }
        else
        {
          this.showusernamenotifbox = false;
          this.showfilenamenotifbox = false;
          this.identityofuploader = this.newname;
          this.itsedit = false;
          console.log(msg);
        }
      });
    }
    else if(!this.newname)
    {
      alert("nothing to submit");
    }
    else
    {
      alert("unknown error");
    }
  }
  public newfilename($event){
    this.newfilenameobj = $event.target.value;
  }
  public submitnewfilename(){
    if(this.newfilenameobj)
    {
      const deliverfileinfo = {
        "filename" : this.newfilenameobj,
        "id" : this.idparam,
      }
      this.socket.filenameupdate(deliverfileinfo);
      this.socket.recieveupdatemsgfilename()
        .subscribe((msg) => {
          if(msg === "error")
          {
            console.log("error");
          }
          else if(msg === "no-response")
          {
            console.log("no response");
          }
          else
          {
            this.showusernamenotifbox = false;
            this.showfilenamenotifbox = false;
            this.filenameitem = this.newfilenameobj;
            this.itsedit = false;
            var tafuta = this.impoarray[0].find(function(element){
              return element._id = msg._id;
            });
            tafuta.display = this.newfilenameobj;
            console.log("found" + tafuta._id);
            console.log(msg);
          }
        });
    }
    else if(!this.newfilenameobj)
    {
      alert("nothing to submit");
    }
    else
    {
      alert("unknown error");
    }

  }
  //function to update coverpic and profile pic

}
