import { Injectable } from "@angular/core";
import * as io from 'socket.io-client';
import { Adress } from "./ip.service";
import { Observable , throwError } from "rxjs";

@Injectable()
 export class SocketService {
   private url = this.outurl.getIp();
   private socket;
   constructor(public outurl : Adress){
     this.socket = io(this.url);
   }
    //emissions
    public nameupdate(newname){
      this.socket.emit("updatename",newname);
    }
    public filenameupdate(file){
      this.socket.emit("filename", file);
    }
    public sthuploaded(fileinfo){
      this.socket.emit("uploadedsth" , fileinfo);
    }
    //emission to set changed image
    public updateCover(imginfo){
      this.socket.emit("updatecoverimg",imginfo);
    }
    //responses
   public recieveupdatesmsg = () => {
     return Observable.create((observer) => {
          this.socket.on('newnameupdatereciever', (message) => {
            observer.next(message);
          });
      });
    }
    public recieveupdatemsgfilename = () => {
      return Observable.create((observer) => {
          this.socket.on('newfilenameupdatereciever' , (message) => {
            observer.next(message);
          });
      });
    }

 }
